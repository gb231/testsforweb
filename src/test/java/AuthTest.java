import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class AuthTest extends AbstractForLogInTest {

     @Test
    @DisplayName("Авторизация пользователя")
    void loginIn() {
         WebElement userName = webDriver.findElement(By.xpath("//input[@type='text']"));
         userName.sendKeys(getUsername());
         WebElement password = webDriver.findElement(By.xpath("//input[@type='password']"));
         password.sendKeys(getPassword());
         WebElement button = webDriver.findElement(By.xpath("//button[@type='submit']"));
         button.click();

           Assertions.assertNotNull(webDriver.findElement(By.cssSelector(".content")));
     }

    @Test
    @DisplayName("Невалидное имя")
    void loginInWithInvalidUserName() throws InterruptedException {

        WebElement userName = webDriver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys(getNotValidUser());
        WebElement password = webDriver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys(getPassword());
        WebElement button = webDriver.findElement(By.xpath("//button[@type='submit']"));
        button.click();
        TimeUnit.SECONDS.sleep(15);
      //  Assertions.assertEquals("401", webDriver.findElement(By.xpath("//h2[@class=\"svelte-uwkxn9\"]")));
      //  Assertions.assertTrue(webDriver.findElement(By.cssSelector(".s-header-item--user")).getText().equals("alinapvr"));
      //  Assertions.assertTrue(webDriver.findElement(By.xpath("//p[@class=\"svelte-uwkxn9\"]")).getText().equals("Invalid credentials"));
    }

    @Test
    @DisplayName("Невалидное имя- 1 символ")
    void loginInWithInvalidUserName1(){
        WebElement userName = webDriver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys("A");
        WebElement password = webDriver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys(getPassword());
        WebElement button = webDriver.findElement(By.xpath("//button[@type='submit']"));
        button.click();
        Assertions.assertNotNull(webDriver.findElement(By.xpath("//p[contains(text(),'Invalid credentials')]")));
    }

    @Test
    @DisplayName("Невалидное имя- 3 символа")
    void invalidShortLogin3(){
        WebElement userName = webDriver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys("Aya");
        WebElement password = webDriver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys(getPassword());
        WebElement button = webDriver.findElement(By.xpath("//button[@type='submit']"));
        button.click();
        Assertions.assertNotNull(webDriver.findElement(By.xpath("//p[contains(text(),'Invalid credentials')]")));
    }


    @Test
    @DisplayName("Невалидное имя- 19 символов")
    void invalidLongLogin19(){
        WebElement userName = webDriver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys("qwertyuasdfghjasdfg");
        WebElement password = webDriver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys(getPassword());
        WebElement button = webDriver.findElement(By.xpath("//button[@type='submit']"));
        button.click();
        Assertions.assertNotNull(webDriver.findElement(By.xpath("//p[contains(text(),'Invalid credentials')]")));
    }

    @Test
    @DisplayName("Невалидное имя-20 символов")
    void invalidLongLogin20(){
        WebElement userName = webDriver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys("zxcvbnmasdfghjqwerty");
        WebElement password = webDriver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys(getPassword());
        WebElement button = webDriver.findElement(By.xpath("//button[@type='submit']"));
        button.click();
        Assertions.assertNotNull(webDriver.findElement(By.xpath("//p[contains(text(),'Invalid credentials')]")));
    }

    @Test
    @DisplayName("Пустое поле Имя")
    void invalidEmptyLogin(){
        WebElement userName = webDriver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys("");
        WebElement password = webDriver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys(getPassword());
        WebElement button = webDriver.findElement(By.xpath("//button[@type='submit']"));
        button.click();
        Assertions.assertNotNull(webDriver.findElement(By.xpath("//p[contains(text(),'Invalid credentials')]")));
    }

}
