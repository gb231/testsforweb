import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class AbstractForLogInTest {

    static WebDriver webDriver;

    private static Long time = 5l;

    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String base_url;
    private static String token;
    private static String notValidPass;
    private static String notValidUser;

    private static String username;
    private static String password;

    @BeforeEach
    void setDriver() throws IOException {

        WebDriverManager.firefoxdriver().setup();
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--incognito");
        options.addArguments("--headless");
        options.addArguments("start-maximized");
        options.setPageLoadTimeout(Duration.ofSeconds(50));
        //options.addArguments("--remote-allow-origins=*"); //For Chrome

        webDriver = new FirefoxDriver(options);
        webDriver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
        webDriver.get("https://test-stand.gb.ru/login");

        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);

        username = prop.getProperty("username");
        password = prop.getProperty("password");
        base_url = prop.getProperty("base_url");
        token = prop.getProperty("token");
        notValidPass = prop.getProperty("notValidPass");
        notValidUser = prop.getProperty("notValidUser");

    }
    public static WebDriver getWebDriver () {
        return webDriver;
    }

    public static String getUsername() {
        return username;
    }

    public static String getPassword() {
        return password;
    }

    public static String getBase_url() {
        return base_url;
    }

    public static String getToken() {
        return token;
    }

    public static String getNotValidPass() {
        return notValidPass;
    }

    public static String getNotValidUser() {
        return notValidUser;
    }

    @AfterEach
    void close () {
        webDriver.quit();
    }

}
