import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class AbstractTest {


    static WebDriver webDriver;
    private static String username;
    private static String password;

    private static Long time = 5l;

    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String base_url;
    private static String token;
    private static String notValidPass;
    private static String notValidUser;


   @BeforeAll
    static void setDriver() throws IOException {
        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);

        username = prop.getProperty("username");
        password = prop.getProperty("password");
        base_url = prop.getProperty("base_url");
        token = prop.getProperty("token");
        notValidPass = prop.getProperty("notValidPass");
        notValidUser = prop.getProperty("notValidUser");


        WebDriverManager.firefoxdriver().setup();
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--incognito");
        options.addArguments("--headless");
        options.addArguments("start-maximized");
        options.setPageLoadTimeout(Duration.ofSeconds(50));

        webDriver = new FirefoxDriver(options);
        webDriver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
        webDriver.get("https://test-stand.gb.ru/login");


        WebElement userName = webDriver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys(

                getUsername());

        WebElement password = webDriver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys(

                getPassword());

        WebElement button = webDriver.findElement(By.xpath("//button[@type='submit']"));
        button.click();


        Assertions.assertNotNull(webDriver.findElement(By.cssSelector(".content")));

    }

    public static String getUsername() {
        return username;
    }

    public static String getPassword() {
        return password;
    }

    public static String getBase_url() {
        return base_url;
    }

    public static String getToken() {
        return token;
    }

    public static String getNotValidPass() {
        return notValidPass;
    }

    public static String getNotValidUser() {
        return notValidUser;
    }


       public static WebDriver getWebDriver () {
           return webDriver;
       }

    @BeforeEach
    void initMainPage(){
        Assertions.assertDoesNotThrow( ()-> getWebDriver().navigate().to("https://test-stand.gb.ru/"),
                "Страница не доступна");
        Assertions.assertTrue(true);

    }


    @AfterAll
    public static void exit(){

        if(webDriver !=null) webDriver.quit();
    }
       }





