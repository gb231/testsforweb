import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class MyHomePageTest extends AbstractTest {


    @Test
    void ClickToNextPage() throws InterruptedException {
        TimeUnit.SECONDS.sleep(15);
        WebElement nextPage = webDriver
                .findElement(By.xpath("//a[contains(text(),'Next Page')]"));
        nextPage.click();
        Assertions.assertNotNull(webDriver.findElement(By.xpath("//a[@href='/?page=2']")));

        }

    @Test
    void ClickToPreviousPage() throws InterruptedException {
        TimeUnit.SECONDS.sleep(10);
        WebElement PreviousPage = webDriver.findElement(By.cssSelector(".svelte-d01pfs.disabled"));
        Assertions.assertNotNull(webDriver.findElement(By.cssSelector(".svelte-d01pfs.disabled")));
        Assertions.assertNotNull(webDriver.findElement(By.xpath("//a[@href='/?page=2']")));
    }

    @Test
    void ClickAboutPage() throws InterruptedException {
        TimeUnit.SECONDS.sleep(15);
        WebElement about = webDriver.findElement(By.xpath("//ul[@class=\"svelte-1rc85o5\"]/li[1]"));
        about.click();
        TimeUnit.SECONDS.sleep(10);
        Assertions.assertNotNull(webDriver.findElement(By.xpath("//h1[contains(text(),'About Page')]")));
    }


    @Test
    void ContactPage() throws InterruptedException {
        TimeUnit.SECONDS.sleep(15);
        WebElement contact = webDriver.findElement(By.xpath("//ul[@class=\"svelte-1rc85o5\"]/li[2]"));
        contact.click();
        Assertions.assertNotNull(webDriver.findElement(By.xpath("//h1[contains(text(),'Contact')]")));
    }

    @Test
    void setNotMyPosts() throws InterruptedException {
        TimeUnit.SECONDS.sleep(10);
        WebElement home = webDriver.findElement(By.cssSelector(".logo.svelte-1rc85o5"));
        home.click();
        WebElement switchBox = webDriver.findElement(By.xpath("//button[@id='SMUI-form-field-1']"));
        switchBox.click();
        TimeUnit.SECONDS.sleep(10);
        Assertions.assertNotNull(webDriver.findElement(By.xpath("//a[@href='/?page=2&owner=notMe']")));
    }

    @Test
    void CreatePost() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        WebElement home = webDriver.findElement(By.cssSelector(".logo.svelte-1rc85o5"));
        home.click();
        WebElement create = webDriver.findElement(By.xpath("//button[@id='create-btn']"));
        TimeUnit.SECONDS.sleep(5);
        create.click();
        Assertions.assertNotNull(webDriver.findElement(By.xpath("//h1[contains(text(),'Create Post')]")));
    }

    @Test

    void ClickNextPageAndPreviousPage() throws InterruptedException {
        TimeUnit.SECONDS.sleep(10);
        WebElement page2 = webDriver.findElement(By.xpath("//a[@href='/?page=2']"));
        page2.click();
        TimeUnit.SECONDS.sleep(10);
        WebElement page1 = webDriver.findElement(By.xpath("//a[@href='/?page=1']"));
        page1.click();
        Assertions.assertNotNull(webDriver.findElement(By.cssSelector(".svelte-d01pfs.disabled")));
    }



    @Test
    @AfterAll
    static void logOut() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        WebElement logOut = webDriver.findElement(By.xpath("//ul[@class=\"svelte-1rc85o5\"]/li[3]"));
        logOut.click();
        TimeUnit.SECONDS.sleep(5);
        WebElement profile = webDriver.findElement(By.xpath("//ul[@class=\"mdc-deprecated-list\"]/li[2]"));
        profile.click();
        Assertions.assertNotNull(webDriver.findElement(By.xpath("//form[@id='login']")));
    }


}
